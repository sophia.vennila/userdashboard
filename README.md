# UserDashboard

## Project Details- version used
Angular CLI: 16.1.7
Node: 18.16.1
Package Manager: npm 9.5.1
Design: Angular Material UI 

### Prerequisites
- Node.js and npm should be installed on your system.

### Installation
1. Clone the repository.
2. Navigate to the UserDashboard directory in the terminal.
3. Run `npm install` to install the project dependencies.

### Running the Angular Project
1. After the installation is complete, run `ng serve` to start the development server.
2. Open your web browser and visit `http://localhost:4200/` to access the application.

### JSON Server API Setup
1. To set up the JSON Server, install it globally by running `npm install -g json-server`.
2. `db.json` is the JSON file with provided data
3. Run the JSON Server using the following command: `json-server --watch db.json`.
4. The JSON Server API will be accessible at `http://localhost:3000/`.


### Navigation Menu
1. Dashboard
2. Users
3. Publications

Dashboard : It has a simple mat card with a "Welcome" message to show routing/navigation
Users : It shows the list of users (userlist component) with a search to filter data.
        On click of specific Name it navigates to UserProfile (user-profile-component)page.
        The company and address details of the user are mapped to the Company Tab (Company-component) and Address Tab (address-component)

Publications: Dummy menu            

### App is built responsive to all types of devices , with design as in wireframe with dynamic data.


