import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../model/user.model';

@Injectable(
  //providedIn: 'root'
)
export class RestapiService {

  constructor(private http:HttpClient) { }
  private apiUrl = 'http://localhost:3000/users';


  GetAllUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.apiUrl).pipe(
      // Use the map operator to transform the JSON data to User[]
      map((data: any) => data as User[])
    );
  }
  

}
