import { Component, OnInit, ViewChild } from '@angular/core';
import { RestapiService } from '../service/restapi.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AfterViewInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  title: any = "Users";
  users = new MatTableDataSource<any>;
  private unsubscribe$ = new Subject<void>();
  filteredValues: any = [];
  constructor(private service: RestapiService) { }

  displayedColumns: string[] = ['name', 'role', 'status', 'publisher'];


  ngOnInit(): void {
    this.GetAllUsers();

  }

  GetAllUsers() {

    this.service.GetAllUsers()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(
        (users) => {
          this.users = new MatTableDataSource(users);

        },
        (error) => {
          console.error('Error fetching users:', error);
        }
      );
  }


  ngAfterViewInit(): void {
    this.users.paginator = this.paginator;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.users.filter = filterValue.trim().toLowerCase();
    if (this.users.paginator) {
      this.users.paginator.firstPage();

    }

  }
  filterby(value: any, event: Event) {
    event.stopPropagation();
    if ((event.target as HTMLInputElement).checked) {
      this.filteredValues.includes(value) && this.filteredValues.length !==0 ? this.removeFromFV(value) : this.filteredValues.push(value);
      this.filterMethod();

    } else {
      this.removeFromFV(value);
    }
    // console.log(value, (event.target as HTMLInputElement).checked)
  }

  filterMethod(){
    if (this.filteredValues.length !== 0) {
      for (let i = 0; i < this.filteredValues.length; i++) {
        console.log(this.filteredValues[i])
        this.users.filter = this.filteredValues[i].trim().toLowerCase();
        if (this.users.paginator) {
          this.users.paginator.firstPage();

        }
      }
    }
    else {
      this.users.filter = ''
    }
  }
  removeFromFV(val: any) {
    const index = this.filteredValues.indexOf(val);
    if (index > -1) { // only splice array when item is found
      this.filteredValues.splice(index, 1); // 2nd parameter means remove one item only
    }
    this.filterMethod();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
