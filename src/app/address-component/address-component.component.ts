import { Component ,Input } from '@angular/core';
import { User } from '../model/user.model';

@Component({
  selector: 'app-address-component',
  templateUrl: './address-component.component.html',
  styleUrls: ['./address-component.component.css']
})
export class AddressComponentComponent {

  @Input() user!: User;

}
