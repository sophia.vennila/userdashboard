
import { Address } from './address.model';; // Import the Address interface or class
import { Company } from './company.model'; // Import the Company interface or class

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
  status:string,
  role:string,
  publisher:string,
  address: Address;
  phone: string;
  website: string;
  company: Company;
}