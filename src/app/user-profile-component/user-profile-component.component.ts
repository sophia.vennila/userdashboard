import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,Route} from '@angular/router';
import { RestapiService } from '../service/restapi.service';
import { User } from '../model/user.model';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-user-profile-component',
  templateUrl: './user-profile-component.component.html',
  styleUrls: ['./user-profile-component.component.css']
})
export class UserProfileComponentComponent implements OnInit{
  id!: number;
  users: User[] = [];
  userdata: any;
  private paramMapSubscription!: Subscription;
  
  constructor(private service: RestapiService,private route: ActivatedRoute) { }
  
  ngOnInit() {

    this.paramMapSubscription = this.route.paramMap.subscribe((params: any) => {
      this.id = Number(params.get('id'));
      this.service.GetAllUsers().subscribe(
        
        (users: User[]) => {
          this.userdata = users.find(user => user.id === this.id);
          //console.log(this.userdata );
      }
      ,
      (error) => {
           console.error('Error fetching users:', error);
      })
    });
      
  }
  

  ngOnDestroy() {
    this.paramMapSubscription.unsubscribe();
  }
}
