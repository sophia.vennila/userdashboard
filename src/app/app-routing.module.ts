import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserListComponent } from './user-list/user-list.component';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UserProfileComponentComponent } from './user-profile-component/user-profile-component.component';

const routes: Routes = [
  
  {component:DashboardComponent,path:""},
  {component:DashboardComponent,path:"dashboard"},
  {component:UserListComponent, path:"user"},
  {component:UserProfileComponentComponent, path:'userprofile/:id'}



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
